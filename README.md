# R Shiny Application Containerization

This repository contains a simple R Shiny application and instructions on how to containerize it using Docker, along with guidance on building the image locally and running the container.

## R Shiny Application

The R Shiny application included in this repository is a basic "Hello World" from this [example](https://github.com/rstudio/shiny-examples/tree/main/001-hello). You can find it in the `app.R` file.

## Dockerfile

The `Dockerfile` provided in this repository specifies the environment and dependencies required to run the R Shiny application within a Docker container.

### Building the image locally

To build the Docker image locally, follow the instructions below:

-Ensure Docker is installed on your system.
-Navigate to the directory containing the Dockerfile and application files.
-Run the following command:
```
docker build -t shiny-docker-demo .
```
If you're using an Apple Silicon Mac like us, run this command instead:
```
docker build --platform linux/x86_64 -t shiny-docker-demo .
```
This will create a new image called shiny-docker-demo.

### Running the container Locally

After building the Docker image, the only thing left to do is to run it inside a container.

To do so, run the following command to run the container locally (works on M1/M2 Macs):
```
docker run -p 8180:8180 shiny-docker-demo
```